	  
window.jQuery(function(){
		
		//Kinetic.pixelRatio = 1;
	  var hover_color = '';	
	  var areas = {};
		var stage = new Kinetic.Stage({
				container: 'psheme-map',
				width: $('#psheme-map').width(),
				height: $('#psheme-map').height()
			}),
			shapesLayer = new Kinetic.Layer(),
			shapesCoverLayer = new Kinetic.Layer(),
			tooltipLayer = new Kinetic.Layer(),
			colors = ['green', 'yellow', 'red'],
			activedAreaId;
		var partinfo = $('#partinfo'),
			part_close = $('#partinfo .close'),
			part_contents = $('#partinfo .contents'),
			part_current = '';

	  var drawShape = function( key, area ){
			
			var cl = area.color;
			switch(cl){
				case 'green': 
					cl = 'transparent';
					hover_color = 'green';
				break;
				case 'yellow': 
					cl = '#ff9a03';
					hover_color = 'yellow';
				break;
				case 'red': 
					cl = '#932020';
					hover_color = 'red';
				break;
			}
			
	  
			var polygon = new Kinetic.Polygon({
					points: area.points,
					fill: cl,//area.color,
					status: area.color,
					opacity: 0.5,
					//custom
					key: key,
					num: area.num,
					m2: area.m2,
					m3: area.m3,
					m4: area.m4,
					m5: area.m5,
					price: area.price
			});
			return polygon;
	  }
	  
	  var drawShapeCover = function( key, area ){
			
			var cl = area.color;
			switch(cl){
				case 'green': 
					cl = '#099158';
				break;
				case 'yellow': 
					cl = '#ff9a03';
				break;
				case 'red': 
					cl = '#cc0101';
				break;
			}
	  
			var polygon = new Kinetic.Polygon({
					points: area.points,
					fill: cl,//area.color,
					status: area.color,
					opacity: 0,
					//custom
					key: key,
					num: area.num,
					m2: area.m2,
					m3: area.m3,
					m4: area.m4,
					m5: area.m5,
					price: area.price,
					iscover:true
			});
		return polygon;
	  }
	  
	  
	  const drawData = function(data){
			
			areas = data;
			
			  // draw areas
			  for(var key in areas) {
				var area = areas[key],
					points = area.points,
					shape = drawShape( key, area ),
					shape_cover = drawShapeCover( key, area );
				shapesLayer.add(shape);
				shapesCoverLayer.add(shape_cover);
  // console.log(area);               
if (area.class != 'undefined' && area.show1 == '1') {
//console.log(area.c);
$("."+area.class+"").addClass("show");
}   
                  
			  }

                stage.add(shapesLayer);
                stage.add(shapesCoverLayer);
                
                stage.on('mouseover', function(evt) {//mouseover
                	var shape = evt.targetNode;
                	if (shape && typeof shape.attrs.iscover != 'undefined' && shape.attrs.iscover == true ) {
                	  //console.log( shape.className );
                	  shape.setOpacity(0.3);
                	  shapesCoverLayer.draw();
                	}
                });
                stage.on('mouseout', function(evt) {
                	var shape = evt.targetNode;
                	if (shape && typeof shape.attrs.iscover != 'undefined' && shape.attrs.iscover == true ) {
                	  shape.setOpacity(0);
                	  shapesCoverLayer.draw();
                	}
                	hidePartInfo();
                });
                
                stage.on('mousemove', function(evt) {
                	var shape = evt.targetNode;
                	  //console.log( shape.className );
                	if (shape) {
                	  var mousePos = stage.getPointerPosition();
                	  var x = mousePos.x + 10;
                	  var y = mousePos.y - 80;
                	  if ( typeof shape.attrs != 'undefined' ){
                		updatePartInfo(x, y, shape.attrs);
                	  }
                	}
                  }); 
	  
	  }
	  
	  var hidePartInfo = function(){
		if ( partinfo.is(':visible') ){
			//console.log('close');
			partinfo.hide();
		}
	  
	  }
	  
	  var updatePartInfo = function(x, y, area) {
		var html = '';
		
		if ( !partinfo.is(':visible') ){
			partinfo.show();
		}
		
		//position
		partinfo.css({
			top: y,
			left: x
		});
		
		
		if ( part_current !== area.key ){
			if ( typeof area.m5 != 'undefined' && area.m5.length > 0 ) html += '<p class="area img"><img src="'+area.m5+'" alt=""></p>';
			if ( typeof area.key != 'undefined' ) html += '<p class="area">' + area.key + '</p>';
			if ( typeof area.m2 != 'undefined' && area.m2.length > 0 ) html += '<p class="area">Площадь: '+area.m2+' сот</p>';
			if ( typeof area.m3 != 'undefined' && area.m3.length > 0 ) html += '<p class="area">'+area.m3+'</p>';
			if ( typeof area.m4 != 'undefined' && area.m4.length > 0 ) html += '<p class=" status"><span>'+area.m4+'</span></p>';
			/*
			if ( typeof area.price != 'undefined' && area.price.length > 0 ) html += '<p class="area">Цена: '+area.price+' $/сот</p>';			
			if ( area.status == 'green' ) html += '<p class="status">свободен</p>';
			if ( area.status == 'yellow' ) html += '<p class="status">зарезервирован</p>';
			if ( area.status == 'red' ) html += '<p class="status">продан</p>';
			*/
			
			if ( html.length > 0 ) part_contents.html( html );
            
		}
		
		
		//текущий участок
		part_current = area.key;
		
	  }

	drawData(window.galleryData)

});	  
