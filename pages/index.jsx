import React from 'react'
import { Layout } from '../src/components/Layout'
import { TopPageSlider } from '../src/components/TopPageSlider'
import { Advantages } from '../src/components/Advantages/Advantages'
import { TourLink } from '../src/components/TourLink'
import { Services } from '../src/components/Services/Services'
import { WidgetWrapper } from '../src/components/WidgetWrapper'
import { HouseBlock } from '../src/components/HouseBlock'
import { TopPageGallery } from '../src/components/TopPageGallery'
import { ProjectBlock } from '../src/components/ProjectsBlock'
import { Contacts } from '../src/components/Contacts'
import { FeedbackForm } from '../src/components/FeedbackForm'
import { LocationMap } from '../src/components/LocationMap'
import {
  ROUTER_PROEKTI_DOMOV,
  ROUTER_PROEZD
} from '../src/constants/routes'
import { configureAnchors } from 'react-scrollable-anchor'
import { GenPlan } from '../src/components/GenPlan'
import { VideoBlock, VideoButton } from '../src/components/VideoBlock'
import styles from './index.css'
import { BEM } from '../src/utils/bem'
import { SectionsBlock } from '../src/components/SectionsBlock'
import { Banner } from '../src/components/Banner'

const bem = new BEM(styles)

class IndexPage extends React.Component {
  componentDidMount () {
    configureAnchors({offset: -145, scrollDuration: 400})
  }

  render () {
    return (
      <Layout>
        <Banner />
        <TopPageSlider />
        <Advantages />
        <TourLink />
        <VideoButton />
        <Services />
        <GenPlan />
        <SectionsBlock />
        <WidgetWrapper
          title="ПРОЕКТЫ ДОМОВ"
          id={ROUTER_PROEKTI_DOMOV}
          className={bem.b('IndexPage').e('title').show()}
        >
          <ProjectBlock
            type="projects"
            mode="small"
          />
        </WidgetWrapper>
        <HouseBlock />
        <TopPageGallery/>
        <Contacts />
        <FeedbackForm />
        <WidgetWrapper
          title="Схема проезда"
          id={ROUTER_PROEZD}
        >
          <LocationMap/>
        </WidgetWrapper>
        <VideoBlock />
      </Layout>
    )
  }
}

export default IndexPage
