import React from 'react'
import Head from 'next/head'
import { BEM } from '../src/utils/bem'
import styles from './tour.css'

const bem = new BEM(styles)

const html = `
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
		<title></title>
		<script type="text/javascript" src="swfobject.js">
		</script>
		<style type="text/css" title="Default">
			body, div, h1, h2, h3, span, p {
				font-family: Verdana,Arial,Helvetica,sans-serif;
				color: #000000; 
			}
			body {
			  font-size: 10pt;
			  background : #ffffff;
                                      background: #ffffff url('../bg.jpg') 50% 0 fixed; 
			}
			table,tr,td {
				font-size: 10pt;
				border-color : #ffffff;
				color: #ffffff; 
				border-style : solid;
				border-width : 5px;
			}
			h1 {
				font-size: 18pt;
                                                margin-bottom: 100px;
			}
			h2 {
				font-size: 14pt;
			}
			.warning {
				font-weight: bold;
			}
		</style>	
	</head>
	<body valign="middle">

		<h1></h1>
		<table align="center" valign="middle">

<tr><td valign="middle">
  <script type="text/javascript" src="/static/tour/swfobject.js">
		<script type="text/javascript">
<!--
	
			var flashvars = {};
			var params = {};
			params.quality = "high";
			params.bgcolor = "#ffffff";
			params.allowscriptaccess = "sameDomain";
			params.allowfullscreen = "true";
			params.base="..";
			var attributes = {};
			attributes.id = "pano";
			attributes.name = "pano";
			attributes.align = "middle";
			swfobject.embedSWF(
				"/static/tour/1.swf", "flashContent", 
				"900", "450", 
				"9.0.0", "expressInstall.swf", 
				flashvars, params, attributes);
//-->
		</script>
		<div id="flashContent">
			<p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" /></a></p>
		</div>
		
	</td></tr>
</table>
	</body>
</html>

`


class Tour extends React.Component {
  componentDidMount () {
    var flashvars = {};
			var params = {};
			params.quality = "high";
			params.bgcolor = "#ffffff";
			params.allowscriptaccess = "sameDomain";
			params.allowfullscreen = "true";
			params.base="..";
			var attributes = {};
			attributes.id = "pano";
			attributes.name = "pano";
			attributes.align = "middle";
			window.swfobject.embedSWF(
        "/static/tour/1.swf", 
        "flashContent", 
        "900", 
        "450", 
        "9.0.0", 
        "expressInstall.swf", 
        flashvars, 
        params, 
        attributes
      );
  }
  render () {
    return (
      <>
        <Head>
          <script src="/static/tour/swfobject.js"></script>
        </Head>
        <div className={bem.b('Tour').show()}>
          <div
            className={bem.b('Tour').e('video').show()}
            id="flashContent"
          >
            <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" /></a></p>
          </div>
        </div>
      </>
    )
  }
}

export default Tour
