import React from 'react'
import { Layout } from '../src/components/Layout'
import { Contacts } from '../src/components/Contacts'
import {FeedbackForm} from '../src/components/FeedbackForm'

class Kontacty extends React.Component {
  render () {
    return (
      <Layout>
        <Contacts />
        <FeedbackForm />
      </Layout>
    )
  }
}

export default Kontacty
