import React from 'react'
import { Layout } from '../src/components/Layout'
import { TopPageGallery } from '../src/components/TopPageGallery'
import { configureAnchors } from 'react-scrollable-anchor'

class IndexPage extends React.Component {
  componentDidMount () {
    configureAnchors({offset: -145, scrollDuration: 400})
  }

  render () {
    return (
      <Layout>
        <TopPageGallery/>
      </Layout>
    )
  }
}

export default IndexPage
