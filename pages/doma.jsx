import React from 'react'
import { Layout } from '../src/components/Layout'
import { ProjectBlock } from '../src/components/ProjectsBlock'
import { configureAnchors } from 'react-scrollable-anchor'
import { WidgetWrapper } from '../src/components/WidgetWrapper'
import {
  ROUTER_PROEKTI_DOMOV
} from '../src/constants/routes'

class IndexPage extends React.Component {
  componentDidMount () {
    configureAnchors({offset: -145, scrollDuration: 400})
  }

  render () {
    return (
      <Layout>
        <WidgetWrapper
          title="ПРОЕКТЫ ДОМОВ"
          id={ROUTER_PROEKTI_DOMOV}
        >
          <ProjectBlock
            type="projects"
            mode="small"
          />
        </WidgetWrapper>
      </Layout>
    )
  }
}

export default IndexPage
