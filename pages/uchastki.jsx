import React from 'react'
import { Layout } from '../src/components/Layout'
import { configureAnchors } from 'react-scrollable-anchor'
import { GenPlan } from '../src/components/GenPlan'

class IndexPage extends React.Component {
  componentDidMount () {
    configureAnchors({offset: -145, scrollDuration: 400})
  }

  render () {
    return (
      <Layout>
        <GenPlan />
      </Layout>
    )
  }
}

export default IndexPage
