import React from 'react'
import { Layout } from '../src/components/Layout'
import { HouseBlock } from '../src/components/HouseBlock'
import { Contacts } from '../src/components/Contacts'
import { FeedbackForm } from '../src/components/FeedbackForm'
import { configureAnchors } from 'react-scrollable-anchor'

class IndexPage extends React.Component {
  componentDidMount () {
    configureAnchors({offset: -145, scrollDuration: 400})
  }

  render () {
    return (
      <Layout>
        <HouseBlock />
        <Contacts />
        <FeedbackForm />
      </Layout>
    )
  }
}

export default IndexPage
