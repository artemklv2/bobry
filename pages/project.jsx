import React from 'react'
import { Layout, LayoutInner } from '../src/components/Layout'
import { proecty } from '../data/proecty'
import { HousePage } from '../src/components/HousePage'
import { ProjectBlock } from '../src/components/ProjectsBlock'

class Project extends React.Component {
  static async getInitialProps ({asPath}) {
    const resp = {}
    try {
      const code = asPath.split('/').pop()
      const project = {
        id: code,
        ...proecty[code]
      }
      resp.project = project
    } catch (e) {
      resp.error = true

    }

    resp.pathname = asPath

    return resp
  }

  render () {
    const {
      project
    } = this.props

    return (
      <Layout>
        <HousePage
          type="projects"
          {...project}
        />
        <LayoutInner>
          <ProjectBlock
            id={project.id}
            type="projects"
            mode="full"
          />
        </LayoutInner>
      </Layout>
    )
  }
}

export default Project
