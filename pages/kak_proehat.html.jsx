import React from 'react'
import { Layout } from '../src/components/Layout'
import { WidgetWrapper } from '../src/components/WidgetWrapper'
import { Contacts } from '../src/components/Contacts'
import { LocationMap } from '../src/components/LocationMap'
import {
  ROUTER_PROEZD
} from '../src/constants/routes'
import { configureAnchors } from 'react-scrollable-anchor'

class IndexPage extends React.Component {
  componentDidMount () {
    configureAnchors({offset: -145, scrollDuration: 400})
  }

  render () {
    return (
      <Layout>
        <Contacts />
        <WidgetWrapper
          title="Схема проезда"
          id={ROUTER_PROEZD}
        >
          <LocationMap/>
        </WidgetWrapper>
      </Layout>
    )
  }
}

export default IndexPage
