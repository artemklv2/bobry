import React from 'react'
import {Layout, LayoutInner} from '../src/components/Layout'
import { houses } from '../data/houses'
import { HousePage } from '../src/components/HousePage'
import { ProjectBlock } from '../src/components/ProjectsBlock'

class House extends React.Component {
  static async getInitialProps ({asPath}) {
    const resp = {}
    try {
      const code = asPath.split('/').pop()
      const house = {
        id: code,
        ...houses[code]
      }

      resp.house = house
    } catch (e) {
      resp.error = true

    }

    resp.pathname = asPath

    return resp
  }

  render () {
    const {
      house
    } = this.props

    return (
      <Layout>
        <HousePage
          type="houses"
          {...house}
        />
        <LayoutInner>
          <ProjectBlock
            id={house.id}
            type="houses"
            mode="full"
          />
        </LayoutInner>
      </Layout>
    )
  }
}

export default House
