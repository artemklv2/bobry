<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ApiMailController extends AbstractController
{
    /**
     * @Route("/api/mail", name="api_mail")
     */
    public function index(Request $request, \Swift_Mailer $mailer)
    {
        $order = json_decode($request->getContent(), true);

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        $data = $this->sendEmail($order, $mailer)
            ? ['status' => 'success']
            : ['status' => 'error']
        ;

        $response->setContent(json_encode($data));

        return $response;
    }

    private function sendEmail ($order, \Swift_Mailer $mailer) {
        $message = (new \Swift_Message('Заказ'))
            ->setFrom('bobry.info@yandex.ru')
            ->setTo('bobrs2008@yandex.ru')
            ->setBody(
                $this->render(
                    'emails/message.html.twig',
                    $order
                ),
                'text/html'
	    );

        return $mailer->send($message);
    }
}
