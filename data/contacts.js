export const contacts = [
  {
    title: 'ОФИС ПРОДАЖ',
    text: `
Екатеринбург <br />
Добролюбова-16, 2 этаж <br /> 
+7 (343) 222-00-85 <br /> 
+7 (343) 344-28-23 <br />
+7 (919) 385-22-95 <br />
bobrs2008@yandex.ru
    `,
    map: `
<iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A1dd0cd42632c8dda0c86d9bf3046e29e159ae07da8c82067ac0f97e9db802400&amp;source=constructor" width="100%" height="283" frameborder="0"></iframe>
`
  },
  {
    title: 'ПОСМОТРЕТЬ ДОМ / УЧАСТОК',
    text: `
Согласовать дату  <br />
и время просмотра <br />
+7 (912) 24-23-593 <br />
Александр Михайлович <br />
координаты для навигатора <br />
56.726859, 60.958675
`,
    map: `
<iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3Aee49dbee3607cb4119c1acd806a063d3cf61470a2c8212442a1aced5149591d1&amp;source=constructor" width="100%" height="283" frameborder="0"></iframe>
`
  }
]
