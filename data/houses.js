const houses = {
  1: {
    title: 'КОЛЬЦЕВАЯ, 47а',
    description: 'Дом от застройщика. Облицовочный кирпич, отделка черновая, двухкамерные стеклопакеты, временная лестница на второй этаж, входная сейф-дверь. Придомовой участок правильной формы. Земля в собственности. Категория земли: земли населенных пунктов. Разрешенное использование: для дачного строительства. Прописка',
    image: '/1/hk47a_0_skidka.jpg',
    images: [
      {
        caption: 'План 1 этажа',
        thumb: '/1/hk47a_1.jpg',
        src: '/1/hk47a_1.jpg'
      },
      {
        caption: 'План 2 этажа',
        thumb: '/1/hk47a_3.jpg',
        src: '/1/hk47a_3.jpg'
      }
    ],
    houseArea: '162 м2, 6771600 р',
    sizeArea: '14.42, 1319430 р',
    foundation: 'Сваи буронабивные, железобетонный ростверк Блоки ФБС',
    walls: 'Блоки БГМ, кирпич, утепление, ж/б перекрытия\n',
    roof: 'Двухскатная из металлочерепицы с наружным водостоком',
    communication: 'Газ и вода подведены к границе участка, электричество заведено в дом, установлен электрощит, асфальтовая дорога до границы участка',
    price: '8 091 030 р',
    prices: [
      {
        title: 'дом 162 м2, цена 6 771 600 р (41 800 р/м2)',
        price: 'участок 14.42 соток, цена 1 319 430 р'
      }
    ],
    newPrice: '7 100 000 р',
  },
  2: {
    title: 'КОЛЬЦЕВАЯ, 49а',
    description: 'СПЕЦИАЛЬНАЯ ЦЕНА НА ДОМ ОТ ЗАСТРОЙЩИКА. Облицовочный кирпич, отделка черновая, двухкамерные стеклопакеты, временная лестница на второй этаж, входная сейф-дверь. Придомовой участок правильной формы. Земля в собственности. Категория земли: земли населенных пунктов. Разрешенное использование: для дачного строительства. Прописка',
    image: '/1/hk49a_0_skidka.jpg',
    images: [
      {
        caption: 'План 1 этажа',
        thumb: '/1/hk49a_1.jpg',
        src: '/1/hk49a_1.jpg'
      },
      {
        caption: 'План 2 этажа',
        thumb: '/1/hk49a_3.jpg',
        src: '/1/hk49a_3.jpg'
      }
    ],
    houseArea: '162 м2, 5654000 р',
    sizeArea: '14.42, 1 319 430 р',
    foundation: 'Сваи буронабивные, железобетонный ростверк Блоки ФБС',
    walls: 'Блоки БГМ, кирпич, утепление, ж/б перекрытия\n',
    roof: 'Двухскатная из металлочерепицы с наружным водостоком',
    communication: 'Газ и вода подведены к границе участка, электричество заведено в дом, установлен электрощит, асфальтовая дорога до границы участка ',
    price: '7 810 245',
    prices: [
      {
        title: 'дом 162 м2, цена 5 654 000 р (34 901 р/м2)',
        price: 'участок 14.42 соток, цена 1 319 430 р'
      },
    ],
    newPrice: '6 973 430 р',
  },
  3: {
    title: 'КОЛЬЦЕВАЯ, 40',
    description: 'Дом от застройщика. Облицовочный кирпич, отделка черновая, двухкамерные стеклопакеты, временная лестница на второй этаж, входная сейф-дверь. Придомовой участок правильной формы. Земля в собственности. Категория земли: земли населенных пунктов. Разрешенное использование: для дачного строительства. Прописка',
    image: '/1/hk40_0_skidka.jpg',
    images: [
      {
        caption: 'План 1 этажа',
        thumb: '/1/hk40_1.jpg',
        src: '/1/hk40_1.jpg'
      },
      {
        caption: 'План 2 этажа',
        thumb: '/1/hk40_3.jpg',
        src: '/1/hk40_3.jpg'
      }
    ],
    houseArea: '162 м2, 6771600 р',
    sizeArea: '16.99, 1554585 р',
    foundation: 'Сваи буронабивные, железобетонный ростверк Блоки ФБС',
    walls: 'Блоки БГМ, кирпич, утепление, ж/б перекрытия',
    roof: 'Двухскатная из металлочерепицы с наружным водостоком',
    communication: 'Газ и вода подведены к границе участка, электричество заведено в дом, установлен электрощит, асфальтовая дорога до границы участка ',
    price: '8 326 185 р',
    prices: [
      {
        title: 'дом 162 м2, цена 6 771 600 р (41 800 р/м2)',
        price: 'участок 16.99 соток, цена 1 554 585 р'
      }
    ],
    newPrice: '7 400 000 р',
  },
  4: {
    title: 'КОЛЬЦЕВАЯ, 42',
    description: 'Дом от застройщика. Облицовочный кирпич, отделка черновая, двухкамерные стеклопакеты, временная лестница на второй этаж, входная сейф-дверь. Придомовой участок правильной формы. Земля в собственности. Категория земли: земли населенных пунктов. Разрешенное использование: для дачного строительства. Прописка',
    image: '/1/hk42_0_skidka.jpg',
    images: [
      {
        caption: 'План 1 этажа',
        thumb: '/1/hk42_1.jpg',
        src: '/1/hk42_1.jpg'
      },
      {
        caption: 'План 2 этажа',
        thumb: '/1/hk42_3.jpg',
        src: '/1/hk42_3.jpg'
      }
    ],
    houseArea: '190 м2, 7638000 р',
    sizeArea: '16.99, 1 554 585 р',
    foundation: 'Сваи буронабивные, железобетонный ростверк Блоки ФБС',
    walls: 'Блоки БГМ, кирпич, утепление, ж/б перекрытия',
    roof: 'Двухскатная из металлочерепицы с наружным водостоком',
    communication: 'Газ и вода подведены к границе участка, электричество заведено в дом, установлен электрощит, асфальтовая дорога до границы участка ',
    price: '9 192 585 р',
    prices: [
      {
        title: 'дом 190 м2, цена 7 638 000 р (40 200 р/м2)',
        price: 'участок 16.99 соток, цена 1 554 585 р'
      }
    ],
    newPrice: '8 063 000 р',
  },
  5: {
    title: 'ГЛ. АЛЛЕЯ, 29',
    description: 'Дом от застройщика. Облицовочный кирпич, отделка черновая, двухкамерные стеклопакеты, временная лестница на второй этаж, входная сейф-дверь. Придомовой участок правильной формы. Земля в собственности. Категория земли: земли населенных пунктов. Разрешенное использование: для дачного строительства. Прописка',
    image: '/1/hg29_0_skidka.jpg',
    images: [
      {
        caption: 'План 1 этажа',
        thumb: '/1/hg29_1.jpg',
        src: '/1/hg29_1.jpg'
      },
      {
        caption: 'План 2 этажа',
        thumb: '/1/hg29_3.jpg',
        src: '/1/hg29_3.jpg'
      }
    ],
    houseArea: '243 м2, 8334900 р',
    sizeArea: '17.17, 1 571 055 р',
    foundation: 'Сваи буронабивные, железобетонный ростверк Блоки ФБС',
    walls: 'Блоки БГМ, кирпич, утепление, ж/б перекрытия',
    roof: 'Двухскатная из металлочерепицы с наружным водостоком',
    communication: 'Газ и вода подведены к границе участка, электричество заведено в дом, установлен электрощит, асфальтовая дорога до границы участка ',
    price: '9 905 955 р',
    prices: [
      {
        title: 'дом с цокольным этажом 243 м2, цена 8 334 900 р (34 300 р/м2)',
        price: 'участок 17.17 соток, цена 1 571 055 р'
      }
    ],
    newPrice: '8 700 000 р',
  }
}
module.exports = {
  houses
}

