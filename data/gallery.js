export const gallery = {
  1: [
    {
      src: 'dom1.jpg',
      thumb: 'dom1-s.jpg'
    },
    {
      src: 'dom3.jpg',
      thumb: 'dom3-s.jpg'
    },
    {
      src: 'dom4.jpg',
      thumb: 'dom4-s.jpg'
    },
    {
      src: 'dom5.jpg',
      thumb: 'dom5-s.jpg'
    },
    {
      src: 'dom6.jpg',
      thumb: 'dom6-s.jpg'
    },
    {
      src: 'dom7.jpg',
      thumb: 'dom7-s.jpg'
    },
    {
      src: 'dom8.jpg',
      thumb: 'dom8-s.jpg'
    },
    {
      src: 'dom10.jpg',
      thumb: 'dom10-s.jpg'
    }
  ],
  2: [
    {
      src: 'playground1.jpg',
      thumb: 'playground1-s.jpg'
    },
    {
      src: 'playground3.jpg',
      thumb: 'playground3-s.jpg'
    },
    {
      src: 'playground5.jpg',
      thumb: 'playground5-s.jpg'
    },
    {
      src: 'playground6.jpg',
      thumb: 'playground6-s.jpg'
    }
  ],
  3: [
    {
      src: 'comfort3.jpg',
      thumb: 'comfort3-s.jpg'
    },
    {
      src: 'comfort4.jpg',
      thumb: 'comfort4-s.jpg'
    },
    {
      src: 'comfort5.jpg',
      thumb: 'comfort5-s.jpg'
    },
    {
      src: 'comfort7.jpg',
      thumb: 'comfort7-s.jpg'
    }
  ],
  4: [
    {
      src: 'river1.jpg',
      thumb: 'river1-s.jpg'
    },
    {
      src: 'river3.jpg',
      thumb: 'river3-s.jpg'
    },
    {
      src: 'river4.jpg',
      thumb: 'river4-s.jpg'
    },
    {
      src: 'river5.jpg',
      thumb: 'river5-s.jpg'
    }
  ],
}
