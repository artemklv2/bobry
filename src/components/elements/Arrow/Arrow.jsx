import React from 'react'
import { BEM } from '../../../utils/bem'
import styles from './Arrow.css'

const bem = new BEM(styles)

const NextArrow = ({onClick}) => (
  <button
    className={bem.b('Arrow').m('next').show()}
    onClick={onClick}
  />
)

const PrevArrow = ({onClick}) => (
  <button
    className={bem.b('Arrow').m('prev').show()}
    onClick={onClick}
  />
)

export {
  NextArrow,
  PrevArrow
}
