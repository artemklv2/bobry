import React from 'react'
import { BEM } from '../../../utils/bem'
import styles from './PageTitle.css'

const bem = new BEM(styles)

function PageTitle ({children}) {
  return (
    <h2
      className={bem.b('PageTitle').show()}
    >
      {children}
    </h2>
  )
}

export { PageTitle }
