import React from 'react'
import { BEM } from '../../../utils/bem'
import styles from './BlockTitle.css'

const bem = new BEM(styles)

class BlockTitle extends React.Component {

  render () {
    const { children, className, ...rest } = this.props
    return (
      <h2
        className={bem.b('BlockTitle').mix(className).show()}
        {...rest}
      >
        {children}
      </h2>
    )
  }
}

export { BlockTitle }
