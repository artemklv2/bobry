import React from 'react'
import Lightbox from 'react-images'
import ScrollableAnchor from 'react-scrollable-anchor'
import { gallery } from '../../../data/gallery'
import { LayoutInner } from '../Layout'
import { TopPageGalleryThumb } from './TopPageGalleryThumb'
import { BEM } from '../../utils/bem'
import styles from './TopPageGallery.css'
import {ROUTER_GALLEREYA} from '../../constants/routes'

const bem = new BEM(styles)

const images = []
let thumbs = []

Object.entries(gallery)
  .forEach(([icon, items]) => {
    thumbs = items.reduce((acc, item) => {
      images.push({src: `/static/images/gallery/${item.src}`})
      acc.push({
        type: 'image',
        thumb: `/static/images/gallery/${item.thumb}`,
        index: images.length -1
      })
      return acc
    }, thumbs)

    thumbs.push({
      type: 'icon',
      thumb: `/static/images/icon-gallery-${icon}.svg`
    })
  })


class TopPageGallery extends React.Component {
  state = {
    currentImageIndex: 0,
    isOpen: false
  }

  openImage = (index) => {
    this.setState({
      currentImageIndex: index,
      isOpen: true
    })
  }

  closeImage = () => {
    this.setState({
      currentImageIndex: 0,
      isOpen: false
    })
  }

  gotoNext = () => {
    const {
      currentImageIndex
    } = this.state

    const index = (currentImageIndex < images.length - 1)
      ? currentImageIndex + 1
      : 0

    this.setState({
      currentImageIndex: index
    })
  }

  gotoPrev = () => {
    const {
      currentImageIndex
    } = this.state

    const index = (currentImageIndex > 0)
      ? currentImageIndex - 1
      : images.length

    this.setState({
      currentImageIndex: index
    })
  }

  render () {
    const {
      currentImageIndex,
      isOpen
    } = this.state

    return (
      <>
        <ScrollableAnchor id={ROUTER_GALLEREYA}>
          <span />
        </ScrollableAnchor>
        <LayoutInner
          className={bem.b('TopPageGallery').show()}
        >
          {thumbs.map((thumb, index) => (
            <TopPageGalleryThumb
              key={index}
              show={this.openImage}
              {...thumb}
            />
          ))}
          <Lightbox
            currentImage={currentImageIndex}
            images={images}
            isOpen={isOpen}
            onClickNext={this.gotoNext}
            onClickPrev={this.gotoPrev}
            onClose={this.closeImage}
          />
        </LayoutInner>
      </>
    )
  }
}

export { TopPageGallery }
