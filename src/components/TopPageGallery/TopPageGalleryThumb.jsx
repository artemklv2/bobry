import React from 'react'
import PropTypes from 'prop-types'
import { BEM } from '../../utils/bem'
import styles from './TopPageGalleryThumb.css'

const bem = new BEM(styles)

class TopPageGalleryThumb extends React.Component {
  static propTypes = {
    index: PropTypes.number,
    thumb: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    show: PropTypes.func.isRequired
  }

  handleOnClick = () => {
    const {
      index,
      show
    } = this.props

    show(index)
  }

  render () {
    const {
      type,
      thumb
    } = this.props

    if (type === 'icon') {
      return (
        <img
          className={bem.b('TopPageGalleryThumb').m('image').show()}
          src={thumb}
        />
      )
    }

    return (
      <button
        onClick={this.handleOnClick}
        className={bem.b('TopPageGalleryThumb').m('thumb').show()}
        style={{
          backgroundImage: `url(${thumb})`
        }}
      />
    )
  }
}

export { TopPageGalleryThumb }
