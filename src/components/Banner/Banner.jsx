import React from 'react'
import { BEM } from '../../utils/bem'
import styles from './Banner.css'

const bem = new BEM(styles)

function Banner () {
  return (
    <>
      <div className={bem.b('Banner').e('small').show()}>
        <img src="/static/banner/banner_mobile.gif" />
      </div>
      <div className={bem.b('Banner').e('gross').show()}>
        <iframe
          style={{
            border: 'none',
            margin: '0',
            padding: '0',
            height: '70px'
          }}
          width="100%" 
          src="/static/banner/index.html"
        />
      </div>
    </>
  )
}

export { Banner }
