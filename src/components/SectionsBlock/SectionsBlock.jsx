import React from 'react'
import { SectionsBlockItem } from './SectionsBlockItem'
import { LayoutInner } from '../Layout'
import { sections } from '../../../data/sections'
import { BEM } from '../../utils/bem'
import styles from './SectionsBlock.css'

const bem = new BEM(styles)

const sectionsArr = sections.filter(item => (item.address && !item.img))

class SectionsBlock extends React.Component {
  state = {
    isOpen: false
  }

  handleOnOpen = () => {
    this.setState({
      isOpen: true
    })
  }

  render() {
    const { isOpen } = this.state
    console.log('isOpen', isOpen)

    return (
      <div
        className={bem.b('SectionsBlock').show()}
      >
        <LayoutInner>
          <div className={bem.b('SectionsBlock').e('items').show()}>
            {sectionsArr.map((item, index) => (
              <SectionsBlockItem
                key={item.id}
                isOpen={index < 2 || isOpen}
                {...item}
              />
            ))}
          </div>
          <button
            className={bem.b('SectionsBlock').e('showAll').m('isOpen', isOpen).show()}
            onClick={this.handleOnOpen}
          >
            Показать все участки
          </button>
        </LayoutInner>
      </div>
    )
  }
}

export { SectionsBlock }
