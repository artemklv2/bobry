import React from 'react'
import { BEM } from '../../utils/bem'
import styles from './SectionsBlockItem.css'

const bem = new BEM(styles)

function SectionsBlockItem (props) {
  const {
    address,
    price,
    isOpen
  } = props

  return (
    <article className={bem.b('SectionsBlockItem').m('isHidden', !isOpen).show()}>
      <h4 
        className={bem.b('SectionsBlockItem').e('title').show()}
        dangerouslySetInnerHTML={{__html: address}}
      />
      <p 
        className={bem.b('SectionsBlockItem').e('info').show()}
        dangerouslySetInnerHTML={{__html: price}}
      />
    </article>
  )
}

export { SectionsBlockItem }
