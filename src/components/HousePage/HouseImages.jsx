import React from 'react'
import PropTypes from 'prop-types'
import Lightbox from 'react-images'
import { BEM } from '../../utils/bem'
import styles from './HouseImages.css'

const bem = new BEM(styles)

class HouseImages extends React.Component {
  static propTypes = {
    images: PropTypes.arrayOf(
      PropTypes.shape({
        title: PropTypes.string,
        thumb: PropTypes.string,
        src: PropTypes.string
      })
    ),
    type: PropTypes.string
  }

  state = {
    currentImageIndex: 0,
    isOpen: false,
    images: this.props.images.map(image => {
      const src = `/static/images/${this.props.type}${image.src}`
      return {
        ...image,
        src
      }
    })
  }

  openImage = (index) => {
    this.setState({
      currentImageIndex: index,
      isOpen: true
    })
  }

  closeImage = () => {
    this.setState({
      currentImageIndex: 0,
      isOpen: false
    })
  }

  gotoNext = () => {
    const {
      currentImageIndex,
      images
    } = this.state

    const index = (currentImageIndex < images.length - 1)
      ? currentImageIndex + 1
      : 0

    this.setState({
      currentImageIndex: index
    })
  }

  gotoPrev = () => {
    const {
      currentImageIndex,
      images
    } = this.state

    const index = (currentImageIndex > 0)
      ? currentImageIndex - 1
      : images.length

    this.setState({
      currentImageIndex: index
    })
  }

  render () {
    const {
      currentImageIndex,
      isOpen,
      images
    } = this.state

    return (
      <div className={bem.b('HouseImages').show()}>
        <div className={bem.b('HouseImages').e('thumbBlock').show()}>
          {images.map(({thumb, caption}, index) => (
            <botton
              key={`${index}`}
              onClick={() => this.openImage(1)}
              className={bem.b('HouseImages').e('thumb').show()}
            >
              <img src={`/static/images/${this.props.type}${thumb}`} />
              <div className={bem.b('HouseImages').e('thumbCaption').show()}>
                <div>{caption}</div>
              </div>
            </botton>
          ))}
        </div>
        <Lightbox
          currentImage={currentImageIndex}
          images={images}
          isOpen={isOpen}
          onClickNext={this.gotoNext}
          onClickPrev={this.gotoPrev}
          onClose={this.closeImage}
        />
      </div>
    )
  }
}

export { HouseImages }
