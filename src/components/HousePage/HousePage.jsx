import React from 'react'
import PropTypes from 'prop-types'
import { PageTitle } from '../elements/PageTitle'
import { LayoutInner } from '../Layout'
import { Attribute } from './Attribute'
import { HouseImages } from './HouseImages'
import { BEM } from '../../utils/bem'
import styles from './HousePage.css'
import { Prices } from './Prices'
import { NewPrice } from './NewPrice'

const bem = new BEM(styles)

function AttributesBlock (props) {
  const {
    type,
    images,
    communication,
    foundation,
    walls,
    roof,
    prices,
    mode,
    newPrice,
  } = props

  if (mode === 'attrs') {
    return (
      <HouseImages type={type} images={images} />
    )
  }

  return (
    <>
      <Attribute title="ФУНДАМЕНТ" value={foundation} />
      <Attribute title="СТЕНЫ, ПЕРЕКРЫТИЯ" value={walls} />
      <Attribute title="КОММУНИКАЦИИ" value={communication} />
      <Attribute title="КРОВЛЯ" value={roof} />
      <Prices prices={prices} />
      <NewPrice newPrice={newPrice} />
    </>
  )
}

class HousePage extends React.Component {
  static propTypes = {
    type: PropTypes.string.isRequired,
    title: PropTypes.string,
    image: PropTypes.string
  }

  render () {
    const {
      type,
      title,
      image,
      description
    } = this.props

    let block1 = 'houses'
    let block2 = 'attrs'

    if (type === 'houses') {
      block1 = 'attrs'
      block2 = 'houses'
    }

    return (
      <article>
        <PageTitle>{title}</PageTitle>
        <LayoutInner>
          <p className={bem.b('HousePage').e('description').show()}>
            {description}
          </p>
        </LayoutInner>
        <LayoutInner
          className={bem.b('HousePage').e('content').show()}
        >
          <div className={bem.b('HousePage').e('leftBlock').show()}>
            <div className={bem.b('HousePage').e('topImage').show()}>
              <img src={`/static/images/${type}${image}`} />
            </div>
            <AttributesBlock mode={block1} {...this.props}  />
          </div>
          <div className={bem.b('HousePage').e('rightBlock').show()}>
            <AttributesBlock mode={block2} {...this.props}  />
          </div>
        </LayoutInner>
        <LayoutInner>
          <a
            href="/kontacty"
            className={bem.b('HousePage').e('orderLink').show()}
          >
            ОСТАВИТЬ ЗАЯВКУ
          </a>
        </LayoutInner>
      </article>
    )
  }
}

export { HousePage }
