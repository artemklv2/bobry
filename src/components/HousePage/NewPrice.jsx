import React from 'react'
import { BEM } from '../../utils/bem'
import styles from './NewPrice.css'

const bem = new BEM(styles)

function NewPrice ({newPrice}) {
  if (Boolean(newPrice)) {
    return (
      <div className={bem.b('NewPrice').show()}>
        Цена со скидкой - <b>{newPrice}</b>
      </div>
    )
  }

  return null
}

export { NewPrice }
