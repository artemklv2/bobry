import React from 'react'
import { BEM } from '../../utils/bem'
import styles from './Prices.css'

const bem = new BEM(styles)

function Prices ({prices}) {
  if (Array.isArray(prices)) {
    return (
      <div className={bem.b('Prices').show()}>
        {prices.map(({title, price}, index) => (
          <div key={index} className={bem.b('Prices').e('item').show()}>
            <div className={bem.b('Prices').e('title').show()}>{title}</div>
            <div className={bem.b('Prices').e('price').show()}>{price}</div>
          </div>
        ))}
      </div>
    )
  }

  return null
}

export { Prices }
