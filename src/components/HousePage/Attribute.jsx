import React from 'react'
import { BEM } from '../../utils/bem'
import styles from './Attribute.css'

const bem = new BEM(styles)

function Attribute ({title, value}) {
  if (!value) {
    return null
  }

  return (
    <div className={bem.b('Attribute').show()}>
      <div className={bem.b('Attribute').e('title').show()}>
        {title}
      </div>
      <div className={bem.b('Attribute').e('value').show()}>
        {value}
      </div>
    </div>
  )
}

export { Attribute }
