import React from 'react'
import { LayoutInner } from '../Layout'
import { BEM } from '../../utils/bem'
import styles from './TourLink.css'
import { ROUTER_TOUR } from '../../constants/routes'

const bem = new BEM(styles)

function TourLink () {
  return (
    <LayoutInner
      className={bem.b('TourLink').e('wrapper').show()}
    >
      <a href={`${ROUTER_TOUR}`} className={bem.b('TourLink').show()}>
        <img height={24} src="/static/images/icon-3d.svg" />
        <span className={bem.b('TourLink').e('text').show()}>ПОСМОТРЕТЬ 3D ТУР</span>
      </a>
    </LayoutInner>
  )
}

export { TourLink }
