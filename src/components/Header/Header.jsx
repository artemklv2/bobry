import React from 'react'
import { BEM } from '../../utils/bem'
import { LayoutInner } from '../Layout'
import { MainMenu } from '../MainMenu'
import { Logo } from './Logo'
import styles from './Header.css'

import {
  ROUTER_CONTACTS,
  ROUTER_TOUR,
  ROUTER_CONTAKTI
} from '../../constants/routes'

const bem = new BEM(styles)

function Header () {
  return (
    <header className={bem.b('Header').show()}>
      <LayoutInner
        className={bem.b('Header').e('inner').show()}
      >
        <Logo />
        <div
          className={bem.b('Header').e('rightBlock').show()}
        >
          <div
            className={bem.b('Header').e('phones').show()}
          >
            <a href="tel:+73432220085">+7 343 222 00 85</a>
            <a href="tel:+79193852295">+7 919 385 22 95</a>
          </div>
          <div
            className={bem.b('Header').e('icons').show()}
          >
            <a
              className={bem.b('Header').e('iconLink').show()}
              href={ROUTER_TOUR}
            >
              <img src="/static/images/icon-tour.svg" />
            </a>
            <a
              className={bem.b('Header').e('iconLink').show()}
              href={`#${ROUTER_CONTAKTI}`}
            >
              <img src="/static/images/icon-contacts.svg" />
            </a>
            <a
              className={bem.b('Header').e('iconLink').show()}
              href={`#${ROUTER_CONTAKTI}`}
            >
              <img src="/static/images/icon-mail.svg" />
            </a>
          </div>
          <MainMenu />
        </div>
      </LayoutInner>
    </header>
  )
}

export { Header }
