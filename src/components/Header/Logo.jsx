import React from 'react'
import styles from './Logo.css'
import { BEM } from '../../utils/bem'

const bem = new BEM(styles)

function Logo () {
  return (
    <a
      className={bem.b('Logo').show()}
      href="/"
    >
      <img src="/static/images/logo_big.png" />
    </a>
  )
}

export { Logo }
