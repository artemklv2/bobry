import React from 'react'
import { sections } from '../../../data/sections'
import { SectionItem } from './SectionItem'
import { ToolTip } from './ToolTip'

class Sections extends React.Component {
  state = {
    currentSectionId: null
  }

  setCurrentSectionId = (id) => {
    console.log(id)
    this.setState({
      currentSectionId: id
    })
  }

  getPosition = (event) => {
    this.setState({
      top: `${event.clientY + 10}px`,
      left: `${event.clientX}px`
    })
  }

  constructor (props) {
    super(props)
    this.state = {
      top: null,
      left: null
    }
  }

  render() {
    const { currentSectionId, top, left } = this.state

    return (
      <>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="980"
          height="665"
          onMouseMove={this.getPosition}
        >
          {sections.map(({id, d}) => (
            <SectionItem
              key={id}
              setCurrentSectionId={this.setCurrentSectionId}
              id={id}
              d={d}
            />
          ))}
        </svg>
        <ToolTip
          currentSectionId={currentSectionId}
          sections={sections}
          top={top}
          left={left}
        />
      </>
    )
  }
}

export { Sections }
