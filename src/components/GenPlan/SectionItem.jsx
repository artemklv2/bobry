import React from 'react'
import PropTypes from 'prop-types'

const options = {
  fill: "#4a90d6",
  fillOpacity: "1",
  stroke: "#222222",
  strokeOpacity: "1",
  strokeWidth: "2",
  strokeDasharray: "none",
  strokeLinejoin: "round",
  strokeLinecap: "butt",
  strokeDashoffset: "",
  fillRule: "nonzero",
  opacity: "0",
  markerStart: "",
  markerMid: "",
  markerEnd: "",
  style: {color: 'rgba(0, 0, 0, 0)'}
}

class SectionItem extends React.Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
    d: PropTypes.string.isRequired,
    setCurrentSectionId: PropTypes.func
  }

  setCurrentItem = () => {
    const {
      id,
      setCurrentSectionId
    } = this.props

    setCurrentSectionId(id)
  }

  removeCurrentItem = () => {
    const {
      setCurrentSectionId
    } = this.props

    setCurrentSectionId(null)
  }

  render () {
    const { id, d } = this.props

    return (
      <path
        onMouseEnter={this.setCurrentItem}
        onMouseLeave={this.removeCurrentItem}
        d={d}
        id={id}
        {...options}
      />
    )
  }
}

export { SectionItem }
