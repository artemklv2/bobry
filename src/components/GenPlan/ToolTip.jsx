import React from 'react'
import PropTypes from 'prop-types'
import { BEM } from '../../utils/bem'
import styles from './ToolTip.css'

const bem = new BEM(styles)

class ToolTip extends React.Component {
  static propTypes = {
    sections: PropTypes.array
  }

  constructor (props) {
    super(props)
    this.state = {
      currentSection: null
    }
  }

  setCurrentSection = () => {
    const currentSection = this.props.currentSectionId
      ? this.props.sections
        .find(section => section.id === this.props.currentSectionId)
      : null
    this.setState({
      currentSection: null
    }, () => setTimeout(() => {
      this.setState({
        currentSection
      })
    }, 300))
  }

  componentDidUpdate(prevProps) {
    if (
      prevProps.currentSectionId !== this.props.currentSectionId
    ) {
      this.setCurrentSection()
    }
  }

  render () {
    const { currentSection } = this.state

    if (
      !currentSection ||
      !currentSection.address
    ) {
      return null
    }

    const { top, left } = this.props

    return (
      <div
        className={bem.b('ToolTip').show()}
        onMouseMove={this.getPosition}
        style={{
          top,
          left
        }}
      >
        {Boolean(currentSection.img) && (
          <div>
            <img src={`/static/images/genplan/${currentSection.img}`} />
          </div>
        )}
        <h4
          className={bem.b('ToolTip').e('address').show()}
          dangerouslySetInnerHTML={{__html: currentSection.address}}
        />
        {Boolean(currentSection.price) && Boolean(currentSection.newPrice) === false && (
          <p
            className={bem.b('ToolTip').e('price').show()}
            dangerouslySetInnerHTML={{__html: currentSection.price}}
          />
        )}
        {Boolean(currentSection.newPrice) && (
          <p
            className={bem.b('ToolTip').e('price').show()}
          >

            <span 
              className={bem.b('ToolTip').e('newPrice').show()}
              dangerouslySetInnerHTML={{__html: currentSection.newPrice}} /> 
            &nbsp;
            &nbsp;
            <span 
              className={bem.b('ToolTip').e('oldPrice').show()}
              dangerouslySetInnerHTML={{__html: currentSection.price}} /> 
          </p>
        )}
      </div>
    )
  }
}

export { ToolTip }
