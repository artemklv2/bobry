import React from 'react'
import styles from './GenPlan.css'
import { BEM } from '../../utils/bem'
import { LayoutInner } from '../Layout'
import { Sections } from './Sections'
import ScrollableAnchor from 'react-scrollable-anchor'
import { ROUTER_UCHASTKI } from '../../constants/routes'

const bem = new BEM(styles)

function GenPlan () {
  return (
    <div className={bem.b('GenPlan').show()}>
      <ScrollableAnchor
        id={ROUTER_UCHASTKI}
      >
        <h3 className={bem.b('GenPlan').e('title').show()}>ГЕНПЛАН ПОСЕЛКА БОБРЫ / УЧАСТКИ НА ПРОДАЖУ</h3>
      </ScrollableAnchor>
      <LayoutInner>
        <div className={bem.b('GenPlan').e('header').show()}>
          <div className={bem.b('GenPlan').e('headerItem').show()}>
            <div>
              <img src="/static/images/genplan/icon-2.svg" />
            </div>
            <p className={bem.b('GenPlan').e('headerItemTitle').show()}>
              Участок <br />на продажу
            </p>
          </div>

          <div className={bem.b('GenPlan').e('headerItem').show()}>
            <div>
              <img src="/static/images/genplan/icon-3.png" />
            </div>
            <p className={bem.b('GenPlan').e('headerItemTitle').show()}>
              Домом <br />на продажу
            </p>
          </div>

          <div className={bem.b('GenPlan').e('headerItem').show()}>
            <div>
              <img src="/static/images/genplan/icon-1.svg" />
            </div>
            <p className={bem.b('GenPlan').e('headerItemTitle').show()}>
              Бронь
            </p>
          </div>
        </div>
        <div className={bem.b('GenPlan').e('plan').show()}>
          <img src="/static/images/genplan/genplan.jpg" />
          <Sections />
        </div>
      </LayoutInner>
    </div>
  )
}

export { GenPlan }
