import React from 'react'
import InputMask from 'react-input-mask'

const PhoneInput = (props) => (
  <InputMask
    mask="+7 (999) 999-99-99"
    {...props}
  >
    {(inputProps) => (
      <input
        type="text"
        {...inputProps}
      />
    )}
  </InputMask>
)

export default PhoneInput
