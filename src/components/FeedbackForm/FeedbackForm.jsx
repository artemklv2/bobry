import React from 'react'
import PropTypes from 'prop-types'
import styles from './FeedbackForm.css'
import { BEM } from '../../utils/bem'
import FormField from './FormField'
import PhoneInput from './PhoneInput'
import { LayoutInner } from '../Layout'
import { sendOrder } from '../../utils/api'

const bem = new BEM(styles)

function getValues (values) {
  return values.reduce((acc, name) => {
    acc[name] = {
      value: '',
      error: '',
      touched: false,
      focused: false
    }
    return acc
  }, {})
}

function validate (values) {
  values.name.error = null
  values.phone.error = null
  values.comment.error = null
  let error = false

  if (!values.name.value) {
    values.name.error = 'Не указано имя'
    error = true
  }
  if (values.name.value.length < 3) {
    values.name.error = 'Имя не может быть меньше трех символов'
    error = true
  }
  if (values.phone.value.replace(/\D/g, '').length < 11) {
    values.phone.error = 'Неправильно указан телефон'
    error = true
  }

  return {
    values,
    error
  }
}

class FeedbackForm extends React.Component {
  state = {
    error: null,
    values: getValues(this.props.values),
    isFetching: false,
    hasError: false,
    isSubmitted: false
  }

  handleOnChange = (event) => {
    const { value, name } = event.target

    const values = Object.entries(this.state.values)
      .map(([key, option]) => {
        if (key === name) {
          option.value = value
        }
        return [key, option]
      })
      .reduce((acc, [key, option]) => {
        acc[key] = option
        return acc
      }, {})

    this.setState(validate(values))
  }

  handleOnFocus = (event) => {
    const { name } = event.target

    const values = Object.entries(this.state.values)
      .map(([key, option]) => {
        option.focused = key === name
        if (key === name) {
          option.touched = true
        }
        return [key, option]
      })
      .reduce((acc, [key, option]) => {
        acc[key] = option
        return acc
      }, {})

    this.setState({values})
  }


  static propTypes = {
    values: PropTypes.array,
  }

  static defaultProps = {
    values: ['name', 'phone', 'comment'],
    isFetching: false
  }

  componentDidMount () {
    this.setState(validate(this.state.values))
  }

  handleSubmit = (event) => {
    event.preventDefault()
    const { values } = this.state
    const order = Object.entries(values)
      .reduce((acc, [key, item]) => {
        acc[key] = item.value
        return acc
      }, {})

    this.submit(order)
  }

  submit = (order) => {
    this.setState({
      isFetching: true
    }, () => {
      sendOrder(order)
        .then(() => this.setState(
          {
            isFetching: false,
            isSubmitted: true
          })
        )
        .catch(err => {
          console.log(err)
          this.setState({
            isFetching: false,
            isSubmitted: true,
            hasError: true
          })
        })
    })
  }

  render () {

    const {
      values,
      error,
      isFetching,
      hasError,
      isSubmitted
    } = this.state

    return (
      <LayoutInner>
        {hasError && isSubmitted && (
          <div className={bem.b('FeedbackForm').e('messageOuter').show()}>
            <div className={bem.b('FeedbackForm').e('messageInner').show()}>
              <h3>Ошибка отправки заявки!</h3>
              <p>Позвоните нам и мы ответим на ваши вопросы.</p>
            </div>
          </div>
        )}
        {!hasError && isSubmitted && (
          <div className={bem.b('FeedbackForm').e('messageOuter').show()}>
            <div className={bem.b('FeedbackForm').e('messageInner').show()}>
              <h3>Ваш вопрос отправлен.</h3>
              <p>Вам обязательно ответят в течение 24 часов.</p>
            </div>
          </div>
        )}
        <form onSubmit={this.handleSubmit}>
          <div className={bem.b('FeedbackForm').e('line').show()}>

            <FormField
              label="Ваше имя"
              isRequired={true}
              values={values}
              onFocus={this.handleOnFocus}
              onChange={this.handleOnChange}
              name="name"
              Component="input"
              type="text"
              autoComplete="off"
            />

            <FormField
              label="Ваш контактный телефон"
              isRequired={true}
              values={values}
              onFocus={this.handleOnFocus}
              onChange={this.handleOnChange}
              name="phone"
              Component={PhoneInput}
            />

          </div>

          <div className={bem.b('FeedbackForm').e('line').show()}>
            <FormField
              label="Тест сообщения"
              values={values}
              onFocus={this.handleOnFocus}
              onChange={this.handleOnChange}
              name="comment"
              Component="textarea"
              autoComplete="off"
            />
          </div>

          <div className={bem.b('FeedbackForm').e('line').m('submit').show()}>
            <button
              className={bem.b('FeedbackForm').e('submit').show()}
              disabled={error || isFetching}
              type="submit"
            >
              Отправить
            </button>
          </div>

        </form>
      </LayoutInner>
    );
  }
}

export { FeedbackForm }
