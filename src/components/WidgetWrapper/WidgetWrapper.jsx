import React from 'react'
import { LayoutInner } from '../Layout'
import { BlockTitle } from '../elements/BlockTitle'
import ScrollableAnchor from 'react-scrollable-anchor'

function Title ({title, id, className}) {
  if (id) {
    return (
      <ScrollableAnchor
        id={id}
        className={className}
      >
        <span>{title}</span>
      </ScrollableAnchor>
    )
  }
  return title
}

function WidgetWrapper ({title, id, children, className}) {
  return (
    <>
      <BlockTitle className={className}>
        <Title
          id={id}
          title={title}
        />
      </BlockTitle>
      <LayoutInner>
        {children}
      </LayoutInner>
    </>
  )
}

export { WidgetWrapper }
