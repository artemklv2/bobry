import React from 'react'
import Slider from 'react-slick'
import { LayoutInner } from '../Layout'
import { services } from '../../../data/services'
import styles from './Services.css'
import { BEM } from '../../utils/bem'
import { PrevArrow, NextArrow } from '../elements/Arrow/Arrow'
import { ROUTER_OUR_PLUS } from '../../constants/routes'

const bem = new BEM(styles)

const settings = {
  className: bem.b('Services').show(),
  nextArrow: <NextArrow/>,
  prevArrow: <PrevArrow/>,
  infinite: true,
  speed: 500,
  slidesToShow: 6,
  slidesToScroll: 1,
  initialSlide: 0,
  responsive: [
    {
      breakpoint: 361,
      settings: {
        slidesToShow: 1
      }
    }
  ]
}

function Services () {
  return (
    <LayoutInner 
      className={bem.b('Services').e('wrapper').show()}
    >
      <Slider
        {...settings}
      >
        {services.map((image, index) => (
          <a
            href={ROUTER_OUR_PLUS}
            key={index}
            className={bem.b('Services').e('item').show()}
          >
            <img
              key={index}
              src={`/static/images/services/${image}`}
            />
          </a>
        ))}
      </Slider>
    </LayoutInner>
  )
}

export { Services }
