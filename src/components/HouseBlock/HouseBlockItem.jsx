import React from 'react'
import { BEM } from '../../utils/bem'
import styles from './HouseBlockItem.css'

const bem = new BEM(styles)

function HouseBlockItem (props) {
  const {
    title,
    image,
    houseArea,
    sizeArea,
    price,
    id,
    newPrice,
  } = props

  return (
    <article className={bem.b('HouseBlockItem').show()}>
      <img
        className={bem.b('HouseBlockItem').e('image').show()}
        src={`/static/images/houses${image}`}
      />
      {Boolean(newPrice) && (
        <p className={bem.b('HouseBlockItem').e('newPrice').show()}>
          <b>Цена со скидкой</b>
            <br />
          {newPrice}
        </p>
      )}
      <h4 className={bem.b('HouseBlockItem').e('title').show()}>{title}</h4>
      <p
        className={bem.b('HouseBlockItem').e('info').show()}
      >
        дом {houseArea}<br />
        участок {sizeArea}<br />
        итого <span>{price}</span>
      </p>
      <a
        className={bem.b('HouseBlockItem').e('link').show()}
        href={`/houses/${id}`}
      >Посмотреть</a>
    </article>
  )
}

export { HouseBlockItem }
