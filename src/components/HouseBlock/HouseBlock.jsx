import React from 'react'
import { HouseBlockItem } from './HouseBlockItem'
import { LayoutInner } from '../Layout'
import ScrollableAnchor from 'react-scrollable-anchor'
import { houses } from '../../../data/houses'
import { BEM } from '../../utils/bem'
import styles from './HouseBlock.css'
import {ROUTER_GOTOVIE_DOMA} from '../../constants/routes'

const bem = new BEM(styles)

const housesArr = Object.entries(houses)
  .map(([id, house]) => ({id, ...house}))

class HouseBlock extends React.Component {
  render() {
    return (
      <div
        className={bem.b('HouseBlock').show()}
      >
        <ScrollableAnchor
          id={ROUTER_GOTOVIE_DOMA}
        >
          <h3 className={bem.b('HouseBlock').e('title').show()}>
            ГОТОВЫЕ ДОМА НА ПРОДАЖУ
          </h3>
        </ScrollableAnchor>
        <LayoutInner
          className={bem.b('HouseBlock').e('items').show()}
        >
          {housesArr.map(house => (
            <HouseBlockItem
              key={house.id}
              {...house}
            />
          ))}
        </LayoutInner>
      </div>
    )
  }
}

export { HouseBlock }
