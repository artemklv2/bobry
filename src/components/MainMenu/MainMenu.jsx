import React from 'react'
import { BEM } from '../../utils/bem'
import styles from './MainMenu.css'
import {
  ROUTER_UCHASTKI,
  ROUTER_GOTOVIE_DOMA,
  ROUTER_PROEKTI_DOMOV,
  ROUTER_GALLEREYA,
  ROUTER_PROEZD,
  ROUTER_CONTAKTI
} from '../../constants/routes'

const items = [
  {
    title: 'участки',
    link: ROUTER_UCHASTKI
  },
  {
    title: 'проекты домов',
    link: ROUTER_PROEKTI_DOMOV
  },
  {
    title: 'готовые дома',
    link: ROUTER_GOTOVIE_DOMA
  },
  {
    title: 'галерея',
    link: ROUTER_GALLEREYA
  },
  {
    title: 'проезд',
    link: ROUTER_PROEZD
  },
  {
    title: 'контакты',
    link: ROUTER_CONTAKTI
  }
]

const bem = new BEM(styles)

class MainMenu extends React.Component {
  state = {
    isOpen: false
  }

  handleOnBurgerClick = () => {
    this.setState({
      isOpen: !this.state.isOpen
    })
  }

  handleOnMenuClick = () => {
    this.setState({
      isOpen: false
    })
  }

  render () {
    const { isOpen } = this.state

    return (
      <>
        <button
          onClick={this.handleOnBurgerClick}
          className={bem.b('MainMenu').e('burger').show()}
        >
          <span className={bem.b('MainMenu').e('burgerLine').m('one').m('isOpen', isOpen).show()} />
          <span className={bem.b('MainMenu').e('burgerLine').m('two').m('isOpen', isOpen).show()} />
          <span className={bem.b('MainMenu').e('burgerLine').m('three').m('isOpen', isOpen).show()} />
        </button>
        <nav
          className={bem.b('MainMenu').m('isOpen', isOpen).show()}
        >
          {items.map(({link, title}) => (
            <a
              key={link}
              href={`/#${link}`}
              onClick={this.handleOnMenuClick}
              className={bem.b('MainMenu').e('item').m(link.substring(1)).show()}
            >
              {title}
            </a>
          ))}
        </nav>
      </>
    )
  }
}

export { MainMenu }
