import React from 'react'
import { slider } from '../../../data/slider'
import Slider from 'react-slick'
import { NextArrow, PrevArrow } from '../elements/Arrow/Arrow'
import { BEM } from '../../utils/bem'
import styles from './TopPageSlider.css'

const bem = new BEM(styles)

function TopPageSlider () {
  return (
    <div className={bem.b('TopPageSlider').show()}>
      <Slider
        height={400}
        autoplay={true}
        dots={false}
        fade={true}
        autoplaySpeed={2000}
        speed={1000}
        adaptiveHeight={false}
        nextArrow={<NextArrow />}
        prevArrow={<PrevArrow />}
      >
        {slider.map((image, index) => (
          <div
            key={`img${index}`}
            className={bem.b('TopPageSlider').e('item').show()}
          >
            <div
              style={{
                backgroundImage: `url('/static/images/slider/${image}')`
              }}
              className={bem.b('TopPageSlider').e('image').show()}
            />
          </div>
        ))}
      </Slider>
    </div>
  )
}

export {
  TopPageSlider
}
