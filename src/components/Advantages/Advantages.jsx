import React from 'react'
import { advantages } from '../../../data/advantages'
import { BEM } from '../../utils/bem'
import styles from './Advantages.css'
import { LayoutInner } from '../Layout'
import { ROUTER_OUR_PLUS } from '../../constants/routes'

const bem = new BEM(styles)

function Advantages () {
  return (
    <LayoutInner>
      <section className={bem.b('Advantages').show()}>
        {advantages.map(({title, text, type}, index) => (
          <a
            href={ROUTER_OUR_PLUS}
            key={index}
            className={bem.b('Advantages').e('item').m(type).show()}
          >
            <h4
              className={bem.b('Advantages').e('itemTitle').show()}
            >{title}</h4>
            <p
              className={bem.b('Advantages').e('itemText').show()}
            >{text}</p>
          </a>
        ))}
      </section>
    </LayoutInner>
  )
}

export { Advantages }
