import React, { useEffect } from 'react'
import Head from 'next/head'
import PropTypes from 'prop-types'
import { Header } from '../Header/Header'
import { Metrika } from './Metrika'
import styles from './Layout.css'
import { BEM } from '../../utils/bem'

const bem = new BEM(styles);

const defaultTitle = 'Коттеджный поселок бизнес-класса «Бобры» - Екатеринбург, c. Косулино. Коттеджи Екатеринбург. Продажа'
const defaultDescription = 'Коттеджный поселок Бобры - современный коттеджный поселок бизнес-класса в 12 км от Екатеринбурга, вб'
const defaultKeywords = 'Коттедж, коттеджи Екатеринбург, коттеджный поселок Бобры, коттеджный поселок Екатеринбург, строительство коттеджного поселка, строительство загородного дома, коттеджы в Косулино. Продажа коттеджа Екат'

const yandexCounter = `
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
   ym(61475554, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
`;

const googleAnalytics = `
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-161998181-1">
</script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-161998181-1'); 
</script>
`;

function Layout ({
  title = defaultTitle,
  description = defaultDescription,
  keywords = defaultKeywords,
  children
}) {
  return (
    <>
      <Head>
        <title>{title}</title>
        <meta name='viewport' content='width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1'/>
        <meta charSet="utf-8" />
        <meta name="description" content={description} />
        <meta name="keywords" content={keywords} />
        <meta name="robots" content="index,follow"/>
        <link rel="shortcut icon" href="/static/images/favicon.png" type="image/x-icon"></link>
        <meta name="yandex-verification" content="c284e0bbe920ae55" />
        <link rel="stylesheet" type="text/css" charSet="UTF-8" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" />
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" />
      </Head>
      <Metrika />
      <Header />
      <div className={bem.b('Layout').show()} />
      {children}
      <footer
        className={bem.b('Layout').e('footer').show()}
      >
        © 2018 Бобры коттеджный поселок на берегу реки
      </footer>
      <div dangerouslySetInnerHTML={{__html: yandexCounter}} />
      <div dangerouslySetInnerHTML={{__html: googleAnalytics}} />
    </>
  )
}

Layout.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  keywords: PropTypes.string
}

export { Layout }
