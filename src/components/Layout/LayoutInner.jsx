import React from 'react'
import PropTypes from 'prop-types'
import { BEM } from '../../utils/bem'
import styles from './LayoutInner.css'

const bem = new BEM(styles)

function LayoutInner ({children, className, Component, ...rest}) {
  return (
    <Component
      {...rest}
      className={bem.b('LayoutInner').mix(className).show()}
    >
      {children}
    </Component>
  )
}

LayoutInner.propTypes = {
  Component: PropTypes.string
}

LayoutInner.defaultProps = {
  Component: 'div'
}

export { LayoutInner }
