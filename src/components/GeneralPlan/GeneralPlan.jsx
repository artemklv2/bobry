import React from 'react'
import data from '../../../data/data.json'

class GeneralPlan extends React.Component {
  componentDidMount() {
    window.galleryData = data
  }

  render () {
    return (
      <div className="width1280">
        <div
          style={{
            width: 1280,
            height: 868,
            position: "relative"
          }}
        >
          <div
            id="psheme-map"
            style={{
               width: 1280,
               height: 868,
               background: "url('/static/images/genplan/psheme4.png') no-repeat scroll 0% 0% transparent",
               position: "absolute",
               zIndex: 88883
            }}
          />
          <div
            id="psheme-map-background"
            style={{
              width: 1280,
              height: 868,
              background: "url('/static/images/genplan/psheme4.jpg') no-repeat scroll 0% 0% transparent",
              position: "absolute",
              zIndex: 88881
            }}
          />
          <div id="partinfo">
            <div className="contents" />
          </div>
        </div>
      </div>
    )
  }
}

export { GeneralPlan }
