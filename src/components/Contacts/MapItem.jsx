import React from 'react'
import { BEM } from '../../utils/bem'
import styles from './MapItem.css'

const bem = new BEM(styles)

function MapItem ({title, text, map}) {
  return (
    <div className={bem.b('MapItem').show()}>
      <div className={bem.b('MapItem').e('content').show()}>
        <h4 className={bem.b('MapItem').e('title').show()}>
          {title}
        </h4>
        <div
          className={bem.b('MapItem').e('text').show()}
          dangerouslySetInnerHTML={{__html: text}}
        />
      </div>
      <div
        className={bem.b('MapItem').e('map').show()}
        dangerouslySetInnerHTML={{__html: map}}
      />
    </div>
  )
}

export { MapItem }
