import React from 'react'
import ScrollableAnchor from 'react-scrollable-anchor'
import { LayoutInner } from '../Layout'
import { contacts } from '../../../data/contacts'
import { MapItem } from './MapItem'
import { BEM } from '../../utils/bem'
import styles from './Contacts.css'
import {ROUTER_CONTAKTI} from '../../constants/routes'

const bem = new BEM(styles)

function Contacts () {
  return (
    <>
      <ScrollableAnchor id={ROUTER_CONTAKTI}>
        <span />
      </ScrollableAnchor>
      <LayoutInner
        className={bem.b('Contacts').show()}
      >
        {contacts.map((contact, index) => (
          <MapItem
            key={index}
            {...contact}
          />
        ))}
      </LayoutInner>
    </>
  )
}

export { Contacts }
