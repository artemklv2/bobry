import React from 'react'
import { BEM } from '../../utils/bem'
import { locationMap } from '../../../data/locationMap'
import styles from './LocationMap.css'
import Lightbox from 'react-images'

const bem = new BEM(styles)

const images = locationMap.map(image => {
  image.src = `/static/images/location-map${image.src}`
  return image
})

class LocationMap extends React.Component {
  state = {
    currentImageIndex: 0,
    isOpen: false
  }

  openImage = (index) => {
    this.setState({
      currentImageIndex: index,
      isOpen: true
    })
  }

  closeImage = () => {
    this.setState({
      currentImageIndex: 0,
      isOpen: false
    })
  }

  gotoNext = () => {
    const {
      currentImageIndex
    } = this.state

    const index = (currentImageIndex < images.length - 1)
      ? currentImageIndex + 1
      : 0

    this.setState({
      currentImageIndex: index
    })
  }

  gotoPrev = () => {
    const {
      currentImageIndex
    } = this.state

    const index = (currentImageIndex > 0)
      ? currentImageIndex - 1
      : images.length

    this.setState({
      currentImageIndex: index
    })
  }

  render () {
    const {
      currentImageIndex,
      isOpen
    } = this.state

    return (
      <div className={bem.b('LocationMap').show()}>
        {images.map(({src, caption}, index) => (
          <a
            key={index}
            className={bem.b('LocationMap').e('thumb').show()}
            onClick={() => this.openImage(index)}
          >
            <img src={src} />
            <p
              className={bem.b('LocationMap').e('thumbCaption').show()}
            >
              {caption}
            </p>
          </a>
        ))}
        <Lightbox
          currentImage={currentImageIndex}
          images={images}
          isOpen={isOpen}
          onClickNext={this.gotoNext}
          onClickPrev={this.gotoPrev}
          onClose={this.closeImage}
        />
      </div>
    )
  }
}

export { LocationMap }
