import React from 'react'
import Modal from 'react-responsive-modal'
import { BEM } from '../../utils/bem'
import styles from './VideoBlock.css'
import { Youtube } from './Youtube'

const bem = new BEM(styles)

class VideoBlock extends React.Component {
  state = {
    isOpen: false
  }

  onOpen = () => {
    this.setState({
      isOpen: true
    })
  }

  onClose = () => {
    this.setState({
      isOpen: false
    })
  }
  
  render () {
    const { isOpen } = this.state
    
    return (
      <div className={bem.b('VideoBlock').show()}>
        {!isOpen && (
          <button 
            onClick={this.onOpen}
            className={bem.b('VideoBlock').e('play').show()}
          >
            <img width="60" src="/static/images/play.svg" />
          </button>
        )}
        <Modal
          open={isOpen}
          onClose={this.onClose}
          overlayClassName={bem.b('VideoBlock').e('overlay').show()}
          bodyOpenClassName={bem.b('VideoBlock').e('body').show()}
          showCloseIcon={false}
        >
          <Youtube
            videoId={'F2UEzC87Cp4'}
          />
        </Modal>
      </div>
    )
  }
}

export { VideoBlock }
