import React from 'react'
import Modal from 'react-responsive-modal'
import { BEM } from '../../utils/bem'
import styles from './VideoButton.css'
import { Youtube } from './Youtube'
import { LayoutInner } from '../Layout'

const bem = new BEM(styles)

class VideoButton extends React.Component {
  state = {
    isOpen: false
  }

  onOpen = () => {
    this.setState({
      isOpen: true
    })
  }

  onClose = () => {
    this.setState({
      isOpen: false
    })
  }
  
  render () {
    const { isOpen } = this.state
    
    return (
      <LayoutInner
        className={bem.b('VideoButton').e('wrapper').show()}
      >
        <button onClick={this.onOpen} className={bem.b('VideoButton').show()}>
          <img height={24} src="/static/images/icon-3d.svg" />
          <span className={bem.b('VideoButton').e('text').show()}>ПОСМОТРЕТЬ ВИДЕО</span>
        </button>
        <Modal
          open={isOpen}
          onClose={this.onClose}
          overlayClassName={bem.b('VideoBlock').e('overlay').show()}
          bodyOpenClassName={bem.b('VideoBlock').e('body').show()}
          showCloseIcon={false}
        >
          <Youtube
            videoId={'F2UEzC87Cp4'}
          />
        </Modal>
      </LayoutInner>
    )
  }
}

export { VideoButton }
