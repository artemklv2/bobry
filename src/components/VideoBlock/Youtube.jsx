import React from 'react'
import styles from './Youtube.css'
import { BEM } from '../../utils/bem';

const bem = new BEM(styles)

class Youtube extends React.Component {
  render () {
    const { videoId } = this.props

    return (
      <iframe 
        className={bem.b('Youtube').show()}
        frameBorder="0" 
        allowFullScreen="1" 
        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
        title="YouTube video player" 
        width="640" 
        height="360" 
        src={`https://www.youtube.com/embed/${videoId}`}
      />
    )
  }
}

export { Youtube }
