import React from 'react'
import PropTypes from 'prop-types'
import { proecty as projects } from '../../../data/proecty'
import { houses } from '../../../data/houses'
import { ProjectBlockItem } from './ProjectBlockItem'
import styles from './ProjectBlock.css'
import { BEM } from '../../utils/bem'

const bem = new BEM(styles)
const projectsArr = Object.entries(projects)
  .map(([id, project]) => ({id, ...project}))

const housesArr = Object.entries(houses)
  .map(([id, house]) => ({id, ...house}))

function filterItems (items, id) {
  return items.filter(item => item.id !== id)
}

class ProjectBlock extends React.Component {
  static propTypes = {
    id: PropTypes.string,
    type: PropTypes.oneOf([
      'projects',
      'houses'
    ]),
    mode: PropTypes.oneOf([
      'full',
      'small'
    ])
  }

  render () {
    const {
      id,
      type,
      mode
    } = this.props

    let items = (type === 'projects')
      ? projectsArr
      : housesArr

    if (id) {
      items = filterItems(items, id)
    }

    return (
      <section className={bem.b('ProjectBlock').show()}>
        {items.map(item => (
          <ProjectBlockItem
            mode={mode}
            key={item.id}
            type={type}
            {...item}
          />
        ))}
      </section>
    )
  }
}

export { ProjectBlock }
