import React from 'react'
import PropTypes from 'prop-types'
import styles from './ProjectBlockItem.css'
import { BEM } from '../../utils/bem'

const bem = new BEM(styles)

class ProjectBlockItem extends React.Component {
  static propTypes = {
    id: PropTypes.string,
    mode: PropTypes.oneOf([
      'full',
      'small'
    ]),
    title: PropTypes.string,
    description: PropTypes.string,
    image: PropTypes.string,
    price: PropTypes.string,
    newPrice: PropTypes.string
  }

  static defaultProps = {
    mode: 'small'
  }

  render () {
    const {
      id,
      mode,
      title,
      description,
      image,
      newPrice,
      type
    } = this.props

    return (
      <article className={bem.b('ProjectBlockItem').show()}>
        <a
          target="__blank"
          href={`/${type}/${id}`}
        className={bem.b('ProjectBlockItem').e('imageBlock').show()}>
          <img
            className={bem.b('ProjectBlockItem').e('image').show()}
            src={`/static/images/${type}${image}`}
          />
          <h4
            className={bem.b('ProjectBlockItem').e('title').show()}
          >{title}</h4>
        </a>
        <p
          className={bem.b('ProjectBlockItem').e('price').show()}
        >{newPrice}</p>
        {(mode === 'full') && (
          <p
            className={bem.b('ProjectBlockItem').e('description').show()}
          >
            {description}
          </p>
        )}
        {(mode === 'full') && (
          <p
            className={bem.b('ProjectBlockItem').e('sberbank').show()}
          >
            Ипотека от Сбербанка
          </p>
        )}
        <a
          className={bem.b('ProjectBlockItem').e('link').show()}
          target="__blank"
          href={`/${type}/${id}`}
        >
          {type === 'houses' ? 'ПОСМОТРЕТЬ ДОМ' : 'ПОСМОТРЕТЬ ПРОЕКТ'}
        </a>
      </article>
    )
  }
}

export { ProjectBlockItem }
