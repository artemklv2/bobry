const API_MAIL = process.env.NODE_ENV === 'production'
  ? 'http://api.donnaolivia.ru/api/mail'
  : 'http://symfony.localhost/api/mail'

async function sendOrder (message) {
  const resp = await fetch(API_MAIL, {
    method: 'POST',
    mode: 'cors',
    header: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(message)
  })
  const json = await resp.json()

  console.log(json)

  if (json.status !== 'success') {
    throw new Error('fail to send order')
  }

  return json
}

export {
  sendOrder
}
