const withCSS = require('@zeit/next-css')
const { houses } = require('./data/houses')
const { proecty } = require('./data/proecty')

module.exports = withCSS({
  cssModules: true,
  cssLoaderOptions: {
    importLoaders: 1,
    localIdentName: "[local]",
  },
  exportPathMap: async function (defaultPathMap) {
    const housesPathMap = Object.keys(houses)
      .reduce((acc, id) => {
        acc[`/houses/${id}`] = {
          page: '/house'
        }
        return acc
      }, {})
    delete defaultPathMap['/house']

    const projectsMap = Object.keys(proecty)
      .reduce((acc, id) => {
        acc[`/projects/${id}`] = {
          page: '/project'
        }
        return acc
      }, {})
    delete defaultPathMap['/project']

    return {
      ...housesPathMap,
      ...projectsMap,
      ...defaultPathMap
    }
  }
})
